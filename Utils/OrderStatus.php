<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 15.12.17
 * Time: 22.22.
 */

namespace Digibank\CommonBundle\Utils;

class OrderStatus
{
    const NEW = 0;
    const PAID = 1;
    const IN_PROGRESS = 2;
    const PARTNER_IN_PROGRESS = 3;
    const COMPLETED = 4;
    const PARTNER_COMPLETED = 5;

    const DECLINED = -1;

    /**
     * @param int $status
     * @return int
     */
    public static function convertStatusForClient(int $status)
    {
        switch ($status) {
            case self::PARTNER_IN_PROGRESS:
                return self::IN_PROGRESS;
            case self::PARTNER_COMPLETED:
                return self::COMPLETED;
            default:
                return $status;
        }
    }
}
