<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 11/13/18
 * Time: 6:02 PM
 */

namespace Digibank\CommonBundle\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;

class OrderValidationException extends CreateOrderException
{
    /**
     * @var ConstraintViolationListInterface
     */
    private $violations;

    public function __construct(ConstraintViolationListInterface $violations)
    {
        parent::__construct('Order validation exception. Order cannot be created in invalid state!');

        $this->violations = $violations;
    }

    /**
     * @return ConstraintViolationListInterface
     */
    public function getViolations(): ConstraintViolationListInterface
    {
        return $this->violations;
    }
}