<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 11/13/18
 * Time: 6:02 PM
 */

namespace Digibank\CommonBundle\Exception;

class CreateOrderException extends \RuntimeException
{
    private $params;

    public function setParams(array $params)
    {
        $this->params = $params;
    }

    public function getParams()
    {
        return $this->params;
    }
}