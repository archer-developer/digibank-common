<?php

namespace Digibank\CommonBundle\Twig;

class OrderExtension extends \Twig_Extension
{
    /**
     * @var array
     */
    private $notCrypt = ['RUB', 'USD'];

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('format_amount', array($this, 'formatAmount')),
        );
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('format_commission', array($this, 'formatCommission')),
            new \Twig_SimpleFilter('format_order_number', array($this, 'formatOrderNumber')),
            new \Twig_SimpleFilter('format_rate', array($this, 'formatRate')),
        );
    }

    /**
     * @param string $orderId
     *
     * @return string
     */
    public function formatOrderNumber($orderId)
    {
        return sprintf('%08d', $orderId);
    }

    /**
     * @param float  $amount
     * @param string $currency
     *
     * @return float
     */
    public function formatAmount($amount, $currency)
    {
        $precession = in_array($currency, $this->notCrypt) ? 2 : 8;

        return number_format($amount, $precession, ',', '');
    }

    /**
     * @param float $amount
     *
     * @return float
     */
    public function formatCommission($amount)
    {
        $precession = 4;

        return number_format($amount, $precession).' %';
    }

    /**
     * @param float $rate
     *
     * @return float
     */
    public function formatRate($rate)
    {
        if ($rate && $rate < 1) {
            return 1 / $rate;
        }

        return $rate;
    }
}
