<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 11/19/18
 * Time: 2:39 PM
 */

namespace Digibank\CommonBundle\Validator\Constraints;

use Digibank\ApiClientBundle\Model\PaymentAccountPair;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\InvalidOptionsException;

class AmountConstraint extends Constraint
{
    protected $accountPair;
    protected $checkLimit;

    public function __construct($options = null)
    {
        parent::__construct($options);

        $this->accountPair = $options['accountPair'];
        if (!$this->accountPair instanceof PaymentAccountPair) {
            throw new InvalidOptionsException('accountPair option must be instance of PaymentAccountPair');
        }

        $this->checkLimit = (bool)$options['checkLimit'];
    }

    public function getRequiredOptions()
    {
        return ['accountPair', 'checkLimit'];
    }

    public function getAccountPair(): PaymentAccountPair
    {
        return $this->accountPair;
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function isCheckLimit(): bool
    {
        return $this->checkLimit;
    }
}