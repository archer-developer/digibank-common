<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 11/19/18
 * Time: 4:19 PM
 */

namespace Digibank\CommonBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class FieldsConstraint extends Constraint
{
    public $emptyFieldMessage = 'empty_field_to_value';
}