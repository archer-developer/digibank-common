<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 11/19/18
 * Time: 4:20 PM
 */

namespace Digibank\CommonBundle\Validator\Constraints;


use Digibank\ApiClientBundle\API\Client;
use Digibank\CommonBundle\Model\Order;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\InvalidArgumentException;

class FieldsConstraintValidator extends ConstraintValidator
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function validate($order, Constraint $constraint)
    {
        if (!$order instanceof Order || !$constraint instanceof FieldsConstraint) {
            throw new InvalidArgumentException('FieldsConstraintValidator must validate Order class instances only and use FieldsConstraint');
        }

        // Должен быть заполнен кошелек назначения (исключая случай наличных)
        $accounts = $this->client->getAccounts();
        $accountTo = $accounts->get($order->getAccountToId());
        if (empty($order->getFieldsTo()) && !$accountTo->getIsCash()) {
            $this->context
                ->buildViolation($constraint->emptyFieldMessage)
                ->addViolation();
        }
    }
}