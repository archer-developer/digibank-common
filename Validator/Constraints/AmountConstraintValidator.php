<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 11/19/18
 * Time: 2:49 PM
 */

namespace Digibank\CommonBundle\Validator\Constraints;

use Digibank\ApiClientBundle\API\Client;
use Digibank\CommonBundle\Model\Order;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\InvalidArgumentException;

class AmountConstraintValidator extends ConstraintValidator
{
    /**
     * @var Client
     */
    private $client;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(Client $client, TranslatorInterface $translator)
    {
        $this->client = $client;
        $this->translator = $translator;
    }

    public function validate($order, Constraint $constraint)
    {
        if (!$order instanceof Order || !$constraint instanceof AmountConstraint) {
            throw new InvalidArgumentException('AmountConstraintValidator must validate Order class instances only and use AmountConstraint');
        }

        // Проверяем сумму продажи на вхождение в разрешенный интервал
        if ($order->getSumFrom() < $constraint->getAccountPair()->getMinAmountFrom()) {
            $this->context
                ->buildViolation($this->translator->trans('invalid_sum_from', [
                    'sum_from' => $order->getSumFrom(),
                    'min_amount_from' => $constraint->getAccountPair()->getMinAmountFrom(),
                ]))
                ->atPath('sumFrom')
                ->addViolation();
        }

        // Проверяем сумму покупки на вхождение в разрешенный интервал
        if (!empty($constraint->getAccountPair()->getMinAmountTo()) && $order->getSumTo() < $constraint->getAccountPair()->getMinAmountTo()) {
            $this->context
                ->buildViolation($this->translator->trans('invalid_min_sum_to', [
                    '%min%' => $constraint->getAccountPair()->getMinAmountTo(),
                ]))
                ->atPath('sumTo')
                ->addViolation();
        }
        if ($constraint->isCheckLimit()) {
            $accounts = $this->client->getAccounts();
            $accountTo = $accounts->get($order->getAccountToId());
            if ($order->getSumTo() > $accountTo->getLimit()) {
                $this->context
                    ->buildViolation($this->translator->trans('invalid_max_sum_to', [
                        '%max%' => $accountTo->getLimit(),
                    ]))
                    ->atPath('sumTo')
                    ->addViolation();
            }
        }

        if (!empty($constraint->getAccountPair()->getMaxAmountTo()) && $order->getSumTo() > $constraint->getAccountPair()->getMaxAmountTo()) {
            $this->context
                ->buildViolation($this->translator->trans('invalid_max_sum_to', [
                    '%max%' => $constraint->getAccountPair()->getMaxAmountTo(),
                ]))
                ->atPath('sumTo')
                ->addViolation();
        }
    }
}