<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 11/13/18
 * Time: 5:35 PM
 */

namespace Digibank\CommonBundle\Model;

class Order
{
    private $accountFromId;
    private $accountToId;
    private $sumFrom;
    private $sumTo;
    private $fieldsFrom;
    private $fieldsTo;
    private $commission;
    private $rate;
    private $rateFactor;

    /**
     * @var \DateTimeInterface
     */
    private $createdAt;

    /**
     * @return mixed
     */
    public function getAccountFromId()
    {
        return $this->accountFromId;
    }

    /**
     * @return mixed
     */
    public function getAccountToId()
    {
        return $this->accountToId;
    }

    /**
     * @return mixed
     */
    public function getSumFrom()
    {
        return $this->sumFrom;
    }

    /**
     * @return mixed
     */
    public function getSumTo()
    {
        return $this->sumTo;
    }

    /**
     * @return mixed
     */
    public function getFieldsTo()
    {
        return $this->fieldsTo;
    }

    /**
     * @return mixed
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * @return mixed
     */
    public function getFieldsFrom()
    {
        return $this->fieldsFrom;
    }

    /**
     * @return mixed
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @return mixed
     */
    public function getRateFactor()
    {
        return $this->rateFactor;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }
}