<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 11/13/18
 * Time: 5:48 PM
 */

namespace Digibank\CommonBundle\Factory;

use Digibank\ApiClientBundle\API\Client;
use Digibank\ApiClientBundle\Model\PaymentAccountPair;
use Digibank\ApiClientBundle\Model\PaymentAccountPairCollection;
use Digibank\CommonBundle\Exception\CreateOrderException;
use Digibank\CommonBundle\Exception\OrderValidationException;
use Digibank\CommonBundle\Model\Order;
use Digibank\CommonBundle\Validator\Constraints\AmountConstraint;
use Digibank\CommonBundle\Validator\Constraints\FieldsConstraint;
use Doctrine\Common\Util\Inflector;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OrderFactory
{
    /**
     * @var Client
     */
    private $client;

    private $checkLimit;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(Client $client, ValidatorInterface $validator)
    {
        $this->client = $client;
        $this->validator = $validator;
    }

    public function setCheckLimitRule(bool $enable): self
    {
        $this->checkLimit = $enable;

        return $this;
    }

    /**
     * @param Request $request
     * @param PaymentAccountPairCollection $accountPairs
     * @return Order
     * @throws \ReflectionException
     */
    public function createOrder(Request $request, PaymentAccountPairCollection $accountPairs): Order
    {
        $order = new Order();

        $fieldsFromRequest = [
            'account_from_id' => true,
            'account_to_id' => true,
            'sum_from' => true,
            'sum_to' => true,
            'fields_from' => false,
            'fields_to' => false,
        ];
        foreach ($fieldsFromRequest as $field => $required) {
            if (!$request->request->has($field) && $required) {
                throw $this->createException('Invalid request: field not found', ['%field%' => $field]);
            }
            $this->setOrderValue($order, Inflector::camelize($field), $request->get($field));
        }

        $pair = null;
        /** @var PaymentAccountPair $to */
        foreach ($accountPairs->get($order->getAccountFromId()) as $to) {
            if ($to->getAccountToId() == $order->getAccountToId()) {
                $pair = $to;
                break;
            }
        }
        if (!$pair instanceof PaymentAccountPair) {
            throw $this->createException('Invalid account pair', [
                'pairs' => $accountPairs,
                'from_id' => $order->getAccountFromId(),
                'to_id' => $order->getAccountToId(),
            ]);
        }

        $this->setOrderValue($order, 'sumTo', $order->getSumFrom() * $pair->getRate());
        $this->setOrderValue($order, 'commission', $pair->getCommission());
        $this->setOrderValue($order, 'rate', $pair->getRate());
        $this->setOrderValue($order, 'rateFactor', $pair->getRateFactor());
        $this->setOrderValue($order, 'createdAt', new \DateTime());

        $violations = $this->validator->validate($order, [
            new AmountConstraint([
                'accountPair' => $pair,
                'checkLimit' => $this->checkLimit,
            ]),
            new FieldsConstraint(),
        ]);
        if (count($violations) > 0) {
            throw new OrderValidationException($violations);
        }

        return $order;
    }

    /**
     * @param Order $order
     * @param $property
     * @param $value
     * @throws \ReflectionException
     */
    private function setOrderValue(Order $order, $property, $value)
    {
        $reflection = new \ReflectionClass($order);

        $prop = $reflection->getProperty($property);
        $prop->setAccessible(true);
        $prop->setValue($order, $value);
        $prop->setAccessible(false);
    }

    private function createException($message, $params = [])
    {
        $e = new CreateOrderException($message);
        $e->setParams($params);

        return $e;
    }
}